<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Post;
use App\Models\Tag;
use App\Models\Category;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function home(){
        $posts = Post::orderBy('created_at','DESC')->take(5)->get();
        $recentPosts = Post::with('category','user')->orderBy('created_at','DESC')->simplePaginate(9);
        $firstPost = $posts->splice(0,2);
        $middlePost = $posts->splice(0,1);
        $lastPost = $posts->splice(0);
        $footerPost = Post::with('category','user')->inRandomOrder()->limit(4)->get();
        $firstFooterPost = $footerPost->splice(0,1);
        $middleFooterPost = $footerPost->splice(0,2);
        $lastFooterPost = $footerPost->splice(0);
        return view('website.home',
                        compact(
                            'posts',
                            'recentPosts',
                            'firstPost',
                            'middlePost',
                            'lastPost',
                            'firstFooterPost',
                            'middleFooterPost',
                            'lastFooterPost')
                        );
    }

    public function about(){
        $user = auth()->user();
        return view('website.about',compact('user'));
    }

    public function post($slug=''){
        $cat         = Post::query()->get()->groupBy('category_id');
        $categories  = Category::all();
        $tags        = Tag::all();
        $post        = Post::with('category','user')->where('slug',$slug)->first();
        $posts       = Post::with('category','user')->where('category_id',$post->category->id)->inRandomOrder()->limit(4)->get() ;
        $relatedPosts = Post::with('category','user')->orderBy('created_at','DESC')->inRandomOrder()->take(4)->get();
        $firstRelatedPost = $posts->splice(0,1);
        $firstRelatedPost2 = $posts->splice(0,2);
        $lastRelatedPost = $posts->splice(0,1);
        return view('website.post',compact(
                                      [
                                        'post',
                                        'posts',
                                        'categories',
                                        'tags',
                                        'cat',
                                        'relatedPosts',
                                        'firstRelatedPost',
                                        'firstRelatedPost2',
                                        'lastRelatedPost'
                                      ]));
    }

    public function category($slug=''){
        $category = Category::where('slug',$slug)->get()->first();
        if ($category) {
        $posts = Post::where('category_id',$category->id)->paginate(9);
        return view('website.category',compact(['category','posts']));
        }else{
          return redirect()->route('website.home');
        }
    }
    
    public function contact(){
        return view('website.contact');
    }
    public function userDetails($slug=''){
        $user = auth()->user();
        $post = Post::with('category','user')->where('slug',$slug)->first();
        return view('website.userDetails',compact('post','user'));
    }
    public function sendMessage(Request $request)
    {
        $rules = [
            'name' => 'required | max:99',
            'email' => 'required | email',
            'message' => 'required | min:100',
        ];
        $contact = Contact::create($request->all());
        return redirect()->back();
    }
}