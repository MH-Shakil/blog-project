<?php

namespace App\Http\Controllers;

use Session;
use Carbon\Carbon;
use App\Models\Tag;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdateRequest;

class PostController extends Controller
{

    public function index()
    {
        $posts = Post::all();
        return view('admin.post.index',compact('posts'));
    }

  
    public function create()
    {
        $data['categories'] = Category::all();
        $data['tags'] = Tag::all();
        return view('admin.post.create')->with($data);
    }

   
    public function store(PostStoreRequest $request)
    {
       $post = $request->validated();
       $post = new Post();
       $post->title = $request->title;
       $post->slug = Str::slug($request->title,'_');
       if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalExtension();
            $file-> move(public_path('public/post'), $filename);
            $post['image']= $filename;
        }
       $post->description = $request->description;
       $post->category_id = $request->category_id;
       $post->user_id =auth()->user()->id;
       $post->published_at = Carbon::now();
       $post->save();
       $post->tags()->attach($request->tags);
       Session::flash('status','Post Create Successfully');
       return redirect('admin/post');



    }


    public function show(Post $post)
    {
        return view('admin.post.show',compact('post'));
    }

 
    public function edit(Post $post)
    {
      $categories= Category::all();
      $tags = Tag::all();
      return view('admin.post.edit',compact('categories','post','tags'));
    }


    public function update(PostUpdateRequest $request,Post $post)
    {
       $data= $request->validated();
       $data['slug'] = Str::slug($request->title,'_');
        $post->tags()->sync($request->tags);
       if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(public_path('public/post'), $filename);
            $data['image']= $filename;
        }else{
            $data['image'] = $post->image;
        }
       
       $post->update($data);
       Session::flash('status','Post Updated Successfully');
       return redirect()->route('post.index');
    }

 
    public function destroy(Post $post)
    {
        if ($post) {
            $post->delete();
            Session::flash('status','Data Successfully Deleted');
            return redirect('admin/post');
    
          }
    }
}
