<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
class PostUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required',Rule::unique('posts')->ignore($this->post)], 
            'image' => 'sometimes| nullable | image',
            'category_id' => 'required',
            'description' => 'required',
        ];
    }

    public function messages(){
        return [
            'image.image' => 'The file not an image',
            'title.unique' => 'This title alrady exist',
        ];
    }
}
