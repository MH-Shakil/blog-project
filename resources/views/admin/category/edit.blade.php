@extends('layouts.admin')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Create Category</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('blog-home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between item-center">
                    <h3 class="card-title">Category Edit - {{$category->name}}</h3>
                    <a href="{{ URL::to('admin/category') }}" class="btn btn-primary">Category List</a>
                </div>
            </div>
<!--             @if (Session::has('status'))
                <p class="alert alert-success">{{ Session::get('status') }}</p>
            @endif -->
            <div class="row">
                <div class="col-md-7">
                    <form action="{{ route('category.update',[$category->id])}}" method="POST">
                        @csrf
                        @method('PUT')
                        @include('admin.category.form-category',['button'=>'Update'])
                    </form>
                </div>
            </div>

        </div>

        <div>
        @endsection
