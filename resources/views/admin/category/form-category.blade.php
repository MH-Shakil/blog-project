<div class="card-body">
    <div class="form-group">
        <label for="exampleInputname">Name</label>
        <input type="text" name="name" class="form-control" id="exampleInputName"
            placeholder="Enter Category Name" value="{{ isset($category) ? $category->name : old('name')}}">
        @error('name')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputDescription">Description</label>
        <textarea name="description" id="" cols="60" rows="4" class="form-control">{{isset($category)? $category->description : old('description')}}</textarea>
    </div>
</div>
<div class="card-footer">
    <button type="submit" class="btn btn-primary">{{$button}}</button>
</div>