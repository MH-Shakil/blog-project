@extends('layouts.admin')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Create tag</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('blog-home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between item-center">
                    <h3 class="card-title">tag Edit - {{$tag->name}}</h3>
                    <a href="{{ URL::to('admin/tag') }}" class="btn btn-primary">tag List</a>
                </div>
            </div>
<!--             @if (Session::has('status'))
                <p class="alert alert-success">{{ Session::get('status') }}</p>
            @endif -->
            <div class="row">
                <div class="col-md-7">
                    <form action="{{ route('tag.update',[$tag->id])}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputname">Name</label>
                                <input type="text" name="name" class="form-control" id="exampleInputName"
                                    placeholder="Enter tag Name" value="{{$tag->name}}">
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputDescription">Description</label>
                                <textarea name="description" id="" cols="60" rows="4" class="form-control" value="{{$tag->description}}"></textarea>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div>
        @endsection
