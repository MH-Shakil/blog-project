<div class="card-body">
    <div class="form-group">
        <label for="exampleInputTitle">Title</label>
        <input type="text" name="title" class="form-control" value="{{ isset($post) ? $post->title : old('title') }}"
            id="exampleInputTitle" placeholder="Enter Post Ttile">
        @error('title')
            <span class="text-danger">{{ $message }}</span>
        @enderror

    </div>
    <div class="form-group">
        <label for="exampleInputCategory">Post Category</label>
        <select name="category_id" id="category" class="form-control" value="{{ old('category') }}">
            <option selected disabled style="display: none;">Select Category</option>
            {{-- @if ($categories->count() > 0)
                        @foreach ($categories as $c)
                            <option value="{{ $c->id }}" {{ isset($post) && $post->category_id == $c->id ? 'selected' : '' }} >
                                {{ $c->name }}
                            </option>
                        @endforeach
                    @else
                    @endif --}}
            @forelse ($categories as $c)
                <option value="{{ $c->id }}"
                    {{ isset($post) && $post->category_id == $c->id ? 'selected' : '' }}>
                    {{ $c->name }}
                </option>
            @empty
            @endforelse
        </select>
        @error('category_id')
            <span class="text-danger">{{ $message }}</span>
        @enderror


      
    </div>
    <div class="form-group">
                @if (isset($post))
                @foreach ($tags as $tag)
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" name="tags[]" id="tag{{$tag->id}}" value="{{$tag->id}}" 
                    @foreach ($post->tags as $t )
                        {{ $tag->id == $t->id ? 'checked' : ''}}
                    @endforeach
                    >
                    <label for="tag{{$tag->id}}" class="custom-control-label">{{$tag->name}}</label>
                </div>
                @endforeach
                @else
                @foreach ($tags as $tag)
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" name="tags[]" id="tag{{$tag->id}}" value="{{$tag->id}}">
                    <label for="tag{{$tag->id}}" class="custom-control-label">{{$tag->name}}</label>
                </div>
                @endforeach
                @endif
                
               
                
            

            {{-- <div class="custom-control custom-checkbox">
                <input class="custom-control-input" type="checkbox" id="customCheckbox1{{$tag->id}}" value="option{{$tag->id}}">
                <label for="customCheckbox1{{$tag->id}}" class="custom-control-label">Custom Checkbox</label>
              </div> --}}
       
    </div>
    <div class="form-group">
        <label for="exampleInputImage">Image</label>
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile" name="image">
            <label class="custom-file-label" for="customFile">Choose file</label>
        </div>
        @error('image')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputDescription">Description</label>
        <textarea name="description" id="" cols="60" rows="4"
            class="form-control">{{ isset($post) ? $post->description : old('description') }}</textarea>
    </div>
    @error('description')
        <span class="text-danger">{{ $message }}</span>
    @enderror
</div>
<div class="card-footer">
    <button type="submit" class="btn btn-primary">{{ $button }}</button>
</div>
