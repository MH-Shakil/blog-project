@extends('layouts.admin')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">post List</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('blog-home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between item-center">
                    <h3 class="card-title">post List</h3>
                    <a href="{{ route('post.create') }}" class="btn btn-primary">Create post</a>
                </div>

                <!-- <div class="card-tools">
                    <ul class="pagination pagination-sm float-right">
                        <li class="page-item"><a class="page-link" href="#">«</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">»</a></li>
                    </ul>
                </div> -->
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 10px">ID</th>
                            <th>Image</th>
                            <th width="400px" style="float: left;text-align: left;">Title</th>
                            <th>Tag's</th>
                            <th>Categroy</th>
                            <th>Author</th>
                            <th style="width: 40px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($posts->count() > 0)
                            @foreach ($posts as $data)
                                <tr>
                                    <td>{{ $data->id }}</td>
                                    <td>
                                      <div style="max-width: 123px; max-height: 70px; overflow: hidden;">
                                        <img src="{{asset('$data->image')}}" class="img-fluid" alt="{{$data->image}}">                                        
                                      </div>
                                    </td>
                                    <td width="400px">{{ $data->title }}</td>
                                    <td>
                                    @foreach ($data->tags as $tag)
                                        <span class="badge badge-primary">{{$tag->name}}</span>
                                    @endforeach
                                    </td>
                                    <td>{{ $data->category->name }}</td>
                                    <td>{{ $data->user ? $data->user->name : '' }}</td>
                                    <td class="d-flex">
                                        <a href="{{ route('post.show', [$data->id]) }}"
                                            class="btn btn-sm btn-success mr-1"><i class="fas fa-eye"></i></a>
                                        <a href="{{ route('post.edit', [$data->id]) }}"
                                            class="btn btn-sm btn-primary mr-1"><i class="fas fa-edit"></i></a>
                                        <form action="{{ route('post.destroy', [$data->id]) }}}" class="mr-1"
                                            method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-sm btn-danger "><i class="fas fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">
                                        <h5 class="text-center">No post found</h5>
                                    </td>
                                </tr>
                            @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    <!-- /.card -->
    </div>
    <div>
    @endsection
