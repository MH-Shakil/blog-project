@extends('layouts.admin')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">post List</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('blog-home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between item-center">
                    <h3 class="card-title">post List</h3>
                    <a href="{{ route('post.index') }}" class="btn btn-primary">Back to Post</a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table">
                        <tr>
                            <th>Image</th>
                            <td>
                                <div style="max-width: 400px; max-height: 400px; overflow: hidden;">
                                    <img src="{{ asset('public/post/' . $post->image) }}" class="img-fluid"
                                        alt="{{ $post->image }}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 10px">ID</th>
                            <td>{{ $post->id }}</td>
                        </tr>
                        <tr>
                            <th>Title</th>
                            <td>{{ $post->title }}</td>
                        </tr>
                        <tr>
                            <th>Tag's</th>
                            <td>
                            @foreach ($post->tags as $tag)
                                <span class="badge badge-primary">{{ $tag->name }}</span>
                            @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Categroy</th>
                            <td>{{ $post->category->name}}</td>
                        </tr> 
                        <tr>
                            <th style="">Description</th>
                            <td>{{ $post->description }}</td>
                        </tr>
                        <tr>
                            <th>Author</th>
                            <td>{{ $post->user ? $post->user->name : '' }}</td>
                        </tr> 
                        <tr>
                            <th>Action</th>
                            <td class="d-flex">
                                <a href="{{ route('post.edit', [$post->id]) }}" class="btn btn-sm btn-primary mr-1"><i
                                        class="fas fa-edit"></i></a>
                                
                                <form action="{{ route('post.destroy', [$post->id]) }}}" class="mr-1"
                                    method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-sm btn-danger "><i class="fas fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    <!-- /.card -->
    </div>
    <div>
    @endsection
