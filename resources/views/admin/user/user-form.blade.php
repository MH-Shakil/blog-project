<div class="card-body">
    <div class="form-group">
        <label for="exampleInputname">Name</label>
        <input type="text" name="name" class="form-control" id="exampleInputName" placeholder="Enter user Name"
            value="{{ isset($user) ? $user->name : old('name')}}">
        @error('name')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputname">Email</label>
        <input type="email" name="email" class="form-control" id="exampleInputName" placeholder="Enter user email"
            value="{{ isset($user) ? $user->email : old('email')}}">
        @error('email')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputname">Password</label>
        <input type="password" name="password" class="form-control" id="exampleInputName"
            placeholder="Enter user password" value="{{ isset($user) ? $user->password : old('password')}}">
        @error('password')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputname">Image</label>
        <input type="file" name="image" class="form-control" id="exampleInputName" placeholder="Enter user image"
            value="{{ isset($user) ? $user->image : old('image')}}">
        @error('image')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputDescription">Description</label>
        <textarea name="description" id="" cols="60" rows="4"
            class="form-control">{{isset($user)? $user->description : old('description')}}</textarea>
    </div>
</div>
<div class="card-footer">
    <button type="submit" class="btn btn-primary">{{$button}}</button>
</div>