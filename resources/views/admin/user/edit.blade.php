@extends('layouts.admin')
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Create user</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('blog-home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard v1</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="card">
        <div class="card-header">
            <div class="d-flex justify-content-between item-center">
                <h3 class="card-title">user Edit - {{$user->name}}</h3>
                <a href="{{ URL::to('admin/user') }}" class="btn btn-primary">user List</a>
            </div>
        </div>
        @if (Session::has('status'))
        <p class="alert alert-success">{{ Session::get('status') }}</p>
        @endif
        <div class="card-body p-0">
            <div class="row">
                <div class="col-12 col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <form action="{{ route('user.update',[$user->id])}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        @include('admin.user.user-form',['button'=>'Update'])
                    </form>
                </div>
            </div>
        </div>
    </div>
        @endsection