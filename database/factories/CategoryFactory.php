<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Category::class;
    public function definition()
    {
        return [
            'name'=> $this->faker->text(10),
            'slug'=> Str::slug($this->faker->word()),
            'description' => $this->faker->text(400),
        ];
    }
}
