<?php

namespace Database\Factories;

use App\Models\Tag;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class TagFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    Protected $model = Tag::class;
    public function definition()
    {
        return [
            'name'=> $this->faker->text(10),
            'slug'=> Str::slug($this->faker->word()),
            'description' => $this->faker->text(300),
        ];
    }
}