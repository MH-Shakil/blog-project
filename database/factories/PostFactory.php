<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Post::class;
    public function definition()
    {
        $category = Category::count() >= 7 ? Category::inRandomOrder()->first()->id: Category::factory();
        return [
            'title'=> $this->faker->sentence(10),
            'slug'=> Str::slug($this->faker->word()),
            'image'=> $this->faker->imageUrl(600,400),
            'description' => $this->faker->text(300),
            'category_id' => $category,
            'user_id' => 1,
        ];
    }
}
