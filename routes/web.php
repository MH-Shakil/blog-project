<?php

use App\Models\Post;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TagController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FrontEndController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\ContactController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});
// Route::get('/post', function () {
//     return view('website.contact');
// });

Auth::routes();
//forntend controller
Route::get('/home',[FrontEndController::class, 'home'])->name('website.home');
Route::get('/about',[FrontEndController::class, 'about'])->name('website.about');
Route::get('/category/{slug?}',[FrontEndController::class, 'category'])->name('website.category');
Route::get('/contact',[FrontEndController::class, 'contact'])->name('website.contact');
Route::post('/contactStore',[FrontEndController::class, 'sendMessage'])->name('contact.store');
Route::get('/post/{slug?}',[FrontEndController::class, 'post'])->name('website.post');
Route::get('/userDetails/{slug?}',[FrontEndController::class, 'userDetails'])->name('website.userDetails');
Route::get('/blog-home',[IndexController::class,'index'])->name('blog-home');



// admin panel route
Route::group(['prefix'=>'admin', 'middleware'=>['auth']], function(){
    Route::get('/test', function () {
        return view('admin.dashboard.index');
    });
    Route::get('/delete',[ContactController::class,'delete'])->name('contact.destroy');
    Route::resource('/category',CategoryController::class);
    Route::resource('/tag',TagController::class);
    Route::resource('/post',PostController::class);
    Route::resource('/user',UserController::class);
    Route::resource('/contacts',ContactController::class);

    Route::get('/profile',[UserController::class,'profile'])->name('user.profile');
    Route::get('/setting',[SettingController::class,'index'])->name('setting.index');
    Route::post('/settingUpdate',[SettingController::class,'update'])->name('setting.update');
    Route::get('/imageCreate',[\App\Http\Controllers\MultipleImageController::class,'create'])->name('multipleImage.create');
    Route::post('/multipleImage',[\App\Http\Controllers\MultipleImageController::class,'Store'])->name('multipleImage.store');
    //message Route

    Route::get('/contactShow/{id}',[\App\Http\Controllers\ContactController::class,'show'])->name('contact.show');
});
Route::get('/logout', function(){
    Auth::logout();
    return Redirect::to('login');
 });
Route::get('/test',function(){
    $posts = Post::all();
    $id =172;
    foreach($posts as $post){
        $post->image = "https://picsum.photos/id/".$id."/640/480.jpg";
        $post->update();
        $id++;
    }
    return $posts;
});
Route::get('/clear', function(){
    \Artisan::call('route:clear');
    \Artisan::call('config:cache');
    \Artisan::call('cache:clear');
});